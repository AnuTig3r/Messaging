Messaging Module
================

![screenshot](https://gitlab.com/francoisjacquet/Messaging/raw/master/screenshot.png?inline=false)

https://www.rosariosis.org/messaging-module/

Version 1.6 - April, 2019

License GNU GPL v2

Author François Jacquet

Sponsored by Whitesystem Colombia

DESCRIPTION
-----------
This module provides an internal messaging system for communication between administrators, teachers, parents and students.

Translated in [French](https://www.rosariosis.org/fr/messaging-module/) & [Spanish](https://www.rosariosis.org/es/messaging-module/).

The **Premium** version adds the following functionalities:

- Attach File to Messages

CONTENT
-------
Messaging
- Messages
- Write

INSTALL
-------
Copy the `Messaging/` folder (if named `Messaging-master`, rename it) and its content inside the `modules/` folder of RosarioSIS.

Go to _School Setup > School Configuration > Modules_ and click "Activate".

Requires RosarioSIS 2.9+
